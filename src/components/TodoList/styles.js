import { styled } from '@material-ui/core/styles';

export default styled('div')({
  maxWidth: '100%',
  width: 300,
});
