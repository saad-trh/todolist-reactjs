import React, { useState, useEffect } from 'react';
import { List } from '@material-ui/core';
import CustomContainer from './styles';
import TodoItem from '../TodoItem';

const TodoList = ({ todos, handleTodoClick, handleTodoEdit, filter, searchValue }) => {
  const [editedTodoID, setEditedTodo] = useState(null);
  const handleClick = (e) => {
    handleTodoClick(e);
  };

  const handleDoubleClick = (e) => {
    const { target } = e;
    const todoItem = target.closest('.todo-item');
    const toggleComplete = target.closest('.todo-item__toggle-complete');

    if (!todoItem || toggleComplete) return;

    setEditedTodo(todoItem.id);
  };

  const handleItemEdit = (e) => {
    setEditedTodo(null);
    handleTodoEdit(e);
  };

  useEffect(() => {
    const inputEditTodo = document.querySelector('#inputEditTodo');

    if (inputEditTodo) inputEditTodo.focus();
  }, [editedTodoID]);

  const getFilteredTodos = () => {
    let todosSearchResult = todos;

    if (searchValue) {
      todosSearchResult = todos.filter((todo) =>
        todo.title.toLowerCase().includes(searchValue.toLowerCase()),
      );
    }

    switch (filter) {
      case 'activeOption':
        return todosSearchResult.filter((todo) => !todo.isComplete);

      case 'completedOption':
        return todosSearchResult.filter((todo) => todo.isComplete);

      default:
        return todosSearchResult;
    }
  };

  return (
    <CustomContainer onClick={handleClick} onDoubleClick={handleDoubleClick} role="presentation">
      <List className="todo-list" component="nav" aria-label="Todo List">
        {getFilteredTodos().map((todo) => (
          <TodoItem
            key={todo.id}
            title={todo.title}
            isComplete={todo.isComplete}
            id={todo.id}
            editedTodoID={editedTodoID}
            handleItemEdit={handleItemEdit}
          />
        ))}
      </List>
    </CustomContainer>
  );
};

export default TodoList;
