import { styled } from '@material-ui/core/styles';
import { ListItem, ListItemText } from '@material-ui/core';

export const CustomListItem = styled(ListItem)({
  minHeight: 58,
  paddingLeft: 72,
});

export const CustomListItemText = styled(ListItemText)({
  textOverflow: 'ellipsis',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
});
