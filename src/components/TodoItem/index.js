import React from 'react';
import {
  ListItem,
  ListItemIcon,
  Checkbox,
  ListItemSecondaryAction,
  IconButton,
  TextField,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { CustomListItem, CustomListItemText } from './styles';

const TodoItem = ({ title, isComplete, id, editedTodoID, handleItemEdit }) => {
  const handleEdit = (e) => {
    if (e.type === 'submit') e.preventDefault();

    handleItemEdit(e);
  };

  const listClassName = `todo-item ${isComplete ? 'todo-item--completed' : ''}`;

  const listContent =
    editedTodoID === id ? (
      <CustomListItem className={listClassName} id={id}>
        <form noValidate autoComplete="off" onSubmit={handleEdit}>
          <TextField id="inputEditTodo" label="" defaultValue={title} onBlur={handleEdit} />
        </form>
      </CustomListItem>
    ) : (
      <ListItem ContainerProps={{ className: listClassName, id }}>
        <ListItemIcon>
          <Checkbox
            edge="start"
            checked={isComplete}
            tabIndex={-1}
            disableRipple
            inputProps={{ 'aria-labelledby': id }}
            className="todo-item__toggle-complete"
          />
        </ListItemIcon>
        <CustomListItemText disableTypography className="todo-item__text" primary={title} />
        <ListItemSecondaryAction>
          <IconButton edge="end" aria-label="delete" className="button todo-item__remove">
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    );

  return <>{listContent}</>;
};

export default TodoItem;
