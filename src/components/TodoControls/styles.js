import { styled } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

export const CustomText = styled(Typography)({
  marginRight: 24,
});

export const CustomContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center',
});
