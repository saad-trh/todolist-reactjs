import React from 'react';
import { FormControl, RadioGroup, FormControlLabel, Radio, Button } from '@material-ui/core';
import { CustomText, CustomContainer } from './styles';

const TodoControls = ({
  leftItems,
  handleFilterClick,
  checkedFilterOption,
  handleClearCompletedClick,
  hideClearCompleted,
}) => {
  const handleChange = (e) => {
    handleFilterClick(e);
  };

  const handleClick = () => {
    handleClearCompletedClick();
  };

  return (
    <CustomContainer className="todo-controls">
      <CustomText>
        {leftItems !== 1 ? `${leftItems} items left` : `${leftItems} item left`}
      </CustomText>
      <FormControl component="fieldset">
        <RadioGroup
          row
          aria-label="gender"
          name="gender1"
          value={checkedFilterOption}
          onChange={handleChange}
        >
          <FormControlLabel value="allOption" control={<Radio />} label="All" />
          <FormControlLabel value="activeOption" control={<Radio />} label="Active" />
          <FormControlLabel value="completedOption" control={<Radio />} label="Completed" />
        </RadioGroup>
      </FormControl>
      {hideClearCompleted ? (
        <Button
          variant="contained"
          color="primary"
          disableElevation
          size="small"
          onClick={handleClick}
        >
          Clear completed
        </Button>
      ) : (
        ''
      )}
    </CustomContainer>
  );
};

export default TodoControls;
