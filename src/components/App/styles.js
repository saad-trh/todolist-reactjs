import { styled } from '@material-ui/core/styles';
import { Paper, Typography } from '@material-ui/core';

export const CustomMain = styled('main')({
  width: '100%',
});

export const CustomPaper = styled(Paper)({
  margin: '44px auto',
  width: 650,
});

export const CustomTypography = styled(Typography)({
  textAlign: 'center',
  whiteSpace: 'pre-wrap',
});
