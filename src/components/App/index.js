import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import SearchAppBar from '../SearchAppBar';
import InputTodo from '../InputTodo';
import TodoList from '../TodoList';
import TodoControls from '../TodoControls';
import { CustomMain, CustomPaper, CustomTypography } from './styles';
import '../../App.css';

const App = () => {
  const [todos, setTodos] = useState([]);
  const [checkedFilterOption, setCheckedFilterOption] = useState('allOption');
  const [searchValue, setSearchValue] = useState('');
  let isToggleAllActive = todos.every((todo) => todo.isComplete);
  let hideClearCompleted = todos.some((todo) => todo.isComplete);
  let isListEmpty = todos.length === 0;

  const handleInputSubmit = (addedTask) => {
    if (!addedTask) return;

    setTodos(
      [
        {
          id: uuidv4(),
          title: addedTask,
          isComplete: false,
        },
      ].concat(todos),
    );

    isToggleAllActive = false;
    isListEmpty = false;
  };

  const handleTodoClick = (e) => {
    const { target } = e;
    const removeButton = target.closest('.todo-item__remove');
    const toggleComplete = target.closest('.todo-item__toggle-complete');
    const todoItem = target.closest('.todo-item');

    if (removeButton) {
      const { id } = todoItem;
      setTodos(todos.filter((todo) => todo.id !== id));
      isListEmpty = todos.length === 0;
    } else if (toggleComplete) {
      const { id } = todoItem;
      setTodos(
        todos.map((todo) => {
          if (todo.id === id) todo.isComplete = !todo.isComplete;

          return todo;
        }),
      );

      isToggleAllActive = todos.every((todo) => todo.isComplete);
    }

    hideClearCompleted = todos.some((todo) => todo.isComplete);
  };

  const handleTodoEdit = (e) => {
    const { target } = e;
    const { id } = target.closest('.todo-item');
    setTodos(
      todos.map((todo) => {
        if (todo.id === id) {
          const value = target.elements ? target.elements.inputEditTodo.value : target.value;

          if (value) todo.title = value;
        }

        return todo;
      }),
    );
  };

  const leftItems = todos.reduce((acc, current) => {
    if (!current.isComplete) acc += 1;

    return acc;
  }, 0);

  const handleFilterClick = (e) => setCheckedFilterOption(e.target.value);

  const handleToggleAllClick = () => {
    setTodos(
      todos.map((todo) => {
        if (!isToggleAllActive) todo.isComplete = true;
        else todo.isComplete = !todo.isComplete;

        return todo;
      }),
    );

    isToggleAllActive = !isToggleAllActive;

    hideClearCompleted = todos.some((todo) => todo.isComplete);
  };

  const handleClearCompletedClick = () => {
    setTodos(todos.filter((todo) => !todo.isComplete));
    hideClearCompleted = false;
    isListEmpty = true;
  };

  const handleSearchChange = (e) => {
    const { value } = e.target;
    setSearchValue(value);
  };

  return (
    <>
      <SearchAppBar handleSearchChange={handleSearchChange} searchValue={searchValue} />
      <CustomPaper className="app">
        <CustomMain className="main">
          <InputTodo
            handleInputSubmit={handleInputSubmit}
            handleToggleAllClick={handleToggleAllClick}
            isToggleAllActive={isToggleAllActive}
            isListEmpty={isListEmpty}
          />
          <TodoList
            todos={todos}
            handleTodoClick={handleTodoClick}
            handleTodoEdit={handleTodoEdit}
            filter={checkedFilterOption}
            searchValue={searchValue}
          />
          {!isListEmpty ? (
            <TodoControls
              leftItems={leftItems}
              handleFilterClick={handleFilterClick}
              checkedFilterOption={checkedFilterOption}
              handleClearCompletedClick={handleClearCompletedClick}
              hideClearCompleted={hideClearCompleted}
            />
          ) : (
            ''
          )}
        </CustomMain>
      </CustomPaper>
      <CustomTypography variant="caption" display="block">
        {`
          Double-click to edit a todo

          This app is built to showcase most of the use case of ReactJS.

          - Features: Add, Remove, Delete, Toggle todo / Filter List / Toggle All / Delete Completed /
            Search in List.
          - React Hooks are used instead of classes.
          - Styled components are used for CSS / Material-ui for design.
        `}
      </CustomTypography>
    </>
  );
};

export default App;
