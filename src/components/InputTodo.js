import React, { useState } from 'react';
import { TextField, IconButton } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const InputTodo = ({ handleInputSubmit, handleToggleAllClick, isToggleAllActive, isListEmpty }) => {
  const [inputTodoValue, setInputTodoValue] = useState('');
  const handleSubmit = (e) => {
    e.preventDefault();
    handleInputSubmit(inputTodoValue);
    setInputTodoValue('');
  };

  const handleClick = () => {
    handleToggleAllClick();
  };

  const handleChange = ({ target }) => setInputTodoValue(target.value);

  return (
    <div className="add-todo-block">
      {!isListEmpty ? (
        <IconButton
          aria-label="Toggle completed"
          className="add-todo-block__toggle-all button"
          type="button"
          onClick={handleClick}
        >
          <ExpandMoreIcon
            className="toggle-all__icon"
            color={isToggleAllActive ? 'inherit' : 'disabled'}
          />
        </IconButton>
      ) : (
        ''
      )}
      <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <TextField
          id="inputTodo"
          label="What needs to be done?"
          value={inputTodoValue}
          onChange={handleChange}
        />
      </form>
    </div>
  );
};

export default InputTodo;
